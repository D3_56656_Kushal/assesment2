const express= require('express')
const utils= require('../utils')
const route=express.Router()

route.get('/',(req,res)=>
{
const conn= utils.getConnection()

conn.query('select * from student',(err,data)=>
{
    res.send(utils.getResult(err,data))
})
})

route.post('/',(req,res)=>
{
    const{name,age}=req.body
const conn= utils.getConnection()

conn.query(`insert into student(name,age)values('${name}',${age})`,(err,data)=>
{
    res.send(utils.getResult(err,data))
})
})

route.put('/:id',(req,res)=>
{
    const{id}=req.params
    const{name,age}=req.body
const conn= utils.getConnection()

conn.query(`update student set name='${name}',age=${age} where id=${id}`,(err,data)=>
{
    res.send(utils.getResult(err,data))
})
})

route.delete('/:id',(req,res)=>
{
    const{id}=req.params
 
const conn= utils.getConnection()

conn.query(`delete from student where id=${id}`,(err,data)=>
{
    res.send(utils.getResult(err,data))
})
})

module.exports=route